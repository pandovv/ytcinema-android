package com.pandovv.youtubecinema.network.deserializers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.pandovv.youtubecinema.network.items.Video
import com.pandovv.youtubecinema.network.responses.VideosResponse
import java.lang.reflect.Type

class VideosDeserializer : JsonDeserializer<VideosResponse> {

    companion object {
        private const val GET_items = "items"
        private const val GET_snippet = "snippet"
        private const val GET_contentDetails = "contentDetails"
        private const val GET_id = "id"
        private const val GET_title = "title"
        private const val GET_channelTitle = "channelTitle"
        private const val GET_duration = "duration"
        private const val GET_thumbnails = "thumbnails"
        private const val GET_quality = "medium"
        private const val GET_url = "url"
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): VideosResponse {
        val videos = VideosResponse()
        val jsonObject = json?.asJsonObject
        val items = jsonObject?.get(GET_items)?.asJsonArray ?: return videos
        for (item in items) {
            val jsonItem = item.asJsonObject
            val jsonSnippet = jsonItem.get(GET_snippet)?.asJsonObject ?: continue
            val videoId = getVideoId(jsonItem) ?: continue
            val title = getTitle(jsonSnippet) ?: continue
            val snippetUrl = getSnippetUrl(jsonSnippet) ?: continue
            val channelTitle = getChannelTitle(jsonSnippet) ?: continue
            val duration = getDuration(jsonItem.get(GET_contentDetails)?.asJsonObject) ?: continue
            videos.add(Video(videoId, title, snippetUrl, channelTitle, parseDuration(duration)))
        }
        return videos
    }

    private fun getVideoId(jsonItem: JsonObject?) = jsonItem?.get(GET_id)?.asString
    private fun getTitle(jsonItem: JsonObject?) = jsonItem?.get(GET_title)?.asString
    private fun getChannelTitle(jsonItem: JsonObject?) = jsonItem?.get(GET_channelTitle)?.asString
    private fun getDuration(jsonItem: JsonObject?) = jsonItem?.get(GET_duration)?.asString
    private fun getSnippetUrl(jsonItem: JsonObject?) = jsonItem
            ?.get(GET_thumbnails)?.asJsonObject
            ?.get(GET_quality)?.asJsonObject
            ?.get(GET_url)?.asString


    private fun parseDuration(str: String): String {
        val output = StringBuilder()
        str.replace("PT", "")
                .replace('H', ':')
                .replace('M', ':')
                .replace("S", "")
                .split(':')
                .map { num ->
                    if (!num.isEmpty()) {
                        var add = "$num:"
                        if (Integer.parseInt(num) < 10)
                            add = "0$add"
                        output.append(add)
                    }
                }
        if (output.length == 3) output.insert(0, "00:")
        return output.substring(0, output.length - 1)
    }

}