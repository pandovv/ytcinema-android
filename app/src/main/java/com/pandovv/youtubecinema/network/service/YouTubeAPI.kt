package com.pandovv.youtubecinema.network.service

import com.pandovv.youtubecinema.network.responses.SearchResponse
import com.pandovv.youtubecinema.network.responses.VideosResponse
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface YouTubeAPI {

    companion object {
        const val API_KEY = "AIzaSyCpWwIAfb7lw2MuRB0M7R0p30AXFNRocOw"
        const val BASE_URL = "https://www.googleapis.com/youtube/v3/"
    }

    @GET("search?key=$API_KEY")
    fun search(
            @Query("q") query: String,
            @Query("pageToken") nextPageToken: String? = null,
            @Query("part") part: String = "id",
            @Query("maxResults") results: Int = 10,
            @Query("type") type: String = "video"
    ): Deferred<SearchResponse>

    @GET("videos?key=$API_KEY")
    fun videos(
            @Query("id") ids: String,
            @Query("part") part: String = "snippet,contentDetails"
    ): Deferred<VideosResponse>

}