package com.pandovv.youtubecinema.network.items

import java.io.Serializable

abstract class Item : Serializable {

    abstract val id: String

}