package com.pandovv.youtubecinema.network.items

class Video(

        override val id: String,
        val title: String,
        val snippetUrl: String,
        val channelTitle: String,
        val duration: String

) : Item()