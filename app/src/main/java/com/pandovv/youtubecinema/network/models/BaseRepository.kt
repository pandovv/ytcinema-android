package com.pandovv.youtubecinema.network.models

import com.pandovv.youtubecinema.application.App
import com.pandovv.youtubecinema.network.items.Item
import com.pandovv.youtubecinema.views.fragments.base.interfaces.FragmentViewModelRepository

abstract class BaseRepository(private val viewModel: FragmentViewModelRepository) {

    protected val service = App.getInstance().service

    protected fun onComplete() {
        viewModel.onRefreshComplete()
    }

    protected fun onError() {
        viewModel.onRefreshError()
    }

    protected fun addItems(items: List<Item>, clear: Boolean) {
        viewModel.addItems(items, clear)
    }

}