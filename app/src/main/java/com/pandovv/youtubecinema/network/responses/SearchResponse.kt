package com.pandovv.youtubecinema.network.responses

class SearchResponse(val nextPageToken: String) : ArrayList<String>()