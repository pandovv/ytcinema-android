package com.pandovv.youtubecinema.network.responses

import com.google.firebase.database.GenericTypeIndicator
import com.pandovv.youtubecinema.network.items.Session

class SessionsResponse : GenericTypeIndicator<HashMap<String, Session>>()