package com.pandovv.youtubecinema.network.responses

import com.pandovv.youtubecinema.network.items.Video

class VideosResponse : ArrayList<Video>()