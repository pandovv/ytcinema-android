package com.pandovv.youtubecinema.network.models

import com.pandovv.youtubecinema.views.fragments.base.interfaces.FragmentViewModelRepository

class HomeRepository(vm: FragmentViewModelRepository) : SearchRepository(vm)