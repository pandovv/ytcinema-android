package com.pandovv.youtubecinema.network.deserializers

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.pandovv.youtubecinema.network.responses.SearchResponse
import java.lang.reflect.Type

class SearchDeserializer : JsonDeserializer<SearchResponse> {

    companion object {
        private const val GET_nextPageToken = "nextPageToken"
        private const val GET_items = "items"
        private const val GET_id = "id"
        private const val GET_videoId = "videoId"
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): SearchResponse {
        val jsonObject = json?.asJsonObject
        val token = getNextPageToken(jsonObject) ?: ""
        val responseSearch = SearchResponse(token)
        val items = getItems(jsonObject) ?: return responseSearch
        for (item in items) responseSearch.add(getVideoId(item.asJsonObject) ?: continue)
        return responseSearch
    }

    private fun getNextPageToken(jsonObject: JsonObject?) = jsonObject?.get(GET_nextPageToken)?.asString
    private fun getItems(jsonObject: JsonObject?) = jsonObject?.get(GET_items)?.asJsonArray
    private fun getVideoId(jsonObject: JsonObject?) = jsonObject?.get(GET_id)?.asJsonObject?.get(GET_videoId)?.asString

}