package com.pandovv.youtubecinema.network.items

class Session() : Item() {

    override var id = ""
    var name = ""
    var password = ""
    var videoTitle = ""
    var videoId = ""
    var snippetUrl = ""
    var playerStatus = ""
    var playerTimeline = 0

    constructor(id: String, name: String, password: String, videoTitle: String, videoId: String, snippetUrl: String, playerStatus: String, playerTimeline: Int) : this() {
        this.id = id
        this.name = name
        this.password = password
        this.videoTitle = videoTitle
        this.videoId = videoId
        this.snippetUrl = snippetUrl
        this.playerStatus = playerStatus
        this.playerTimeline = playerTimeline
    }

}
