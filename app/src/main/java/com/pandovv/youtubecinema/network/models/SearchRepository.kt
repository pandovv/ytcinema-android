package com.pandovv.youtubecinema.network.models

import com.pandovv.youtubecinema.views.fragments.base.interfaces.FragmentViewModelRepository
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.android.Main
import kotlinx.coroutines.experimental.launch

open class SearchRepository(vm: FragmentViewModelRepository) : BaseRepository(vm) {

    companion object {
        private const val DEFAULT_PAGE_TOKEN = ""
        private const val IDS_SEPARATOR = ","
    }

    private var nextPageToken = DEFAULT_PAGE_TOKEN

    fun search(query: String, clear: Boolean) {
        GlobalScope.launch(Dispatchers.Main) {
//            try {
//
//            } catch () {
//
//            }
            val search = service.search(query, nextPageToken).await()
            val videos = service.videos(search.joinToString(IDS_SEPARATOR)).await()
            nextPageToken = if (clear) DEFAULT_PAGE_TOKEN else search.nextPageToken
            addItems(videos, clear)
            onComplete()
        }
    }

}