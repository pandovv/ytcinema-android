package com.pandovv.youtubecinema.application

import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.database.DatabaseReference
import com.pandovv.youtubecinema.network.service.YouTubeAPI

interface SingletonImpl {

    val service: YouTubeAPI

    val googleSignInClient: GoogleSignInClient

    val sessionDatabase: DatabaseReference

}