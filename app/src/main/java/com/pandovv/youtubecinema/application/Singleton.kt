package com.pandovv.youtubecinema.application

import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.Scope
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.network.deserializers.SearchDeserializer
import com.pandovv.youtubecinema.network.deserializers.VideosDeserializer
import com.pandovv.youtubecinema.network.responses.SearchResponse
import com.pandovv.youtubecinema.network.responses.VideosResponse
import com.pandovv.youtubecinema.network.service.YouTubeAPI
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Singleton(private val app: AppImpl) : SingletonImpl {

    companion object {
        private const val YOUTUBE_DATA_API = "https://www.googleapis.com/auth/youtube"
        private const val SESSIONS_REFERENCE = "sessions"
    }

    private var mRepo: YouTubeAPI? = null

    private var mGoogleSignInClient: GoogleSignInClient? = null

    override val googleSignInClient: GoogleSignInClient
        get() {
            if (mGoogleSignInClient == null) {
                mGoogleSignInClient = initGoogleSignInClient()
            }
            return mGoogleSignInClient!!
        }

    override val service: YouTubeAPI
        get() {
            if (mRepo == null) {
                mRepo = initRepo()
            }
            return mRepo!!
        }

    override val sessionDatabase: DatabaseReference
        get() = FirebaseDatabase.getInstance().getReference(SESSIONS_REFERENCE)

    private fun initGoogleSignInClient() =
            GoogleSignIn.getClient(app.context,
                    GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestIdToken(app.context.getString(R.string.default_web_client_id))
                            .requestScopes(Scope(YOUTUBE_DATA_API))
                            .requestEmail()
                            .build())

    private fun initRepo() =
            Retrofit.Builder()
                    .baseUrl(YouTubeAPI.BASE_URL)
                    .addConverterFactory(buildConverterFactory())
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .build()
                    .create(YouTubeAPI::class.java)

    private fun buildConverterFactory() = GsonConverterFactory.create(GsonBuilder()
            .registerTypeAdapter(SearchResponse::class.java, SearchDeserializer())
            .registerTypeAdapter(VideosResponse::class.java, VideosDeserializer())
            .create())

}