package com.pandovv.youtubecinema.application

import android.app.Application
import android.content.Context

class App : Application(), AppImpl {

    companion object {

        private lateinit var mSingleton: SingletonImpl

        fun getInstance() = mSingleton

    }

    override val context: Context
        get() = applicationContext

    override fun onCreate() {
        super.onCreate()
        mSingleton = Singleton(this)
    }

}