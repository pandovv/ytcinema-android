package com.pandovv.youtubecinema.views.fragments.preview

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.pandovv.youtubecinema.views.viewModel.LifecycleViewModel

class PreviewViewModel(private val fragment: PreviewFragmentImpl) : LifecycleViewModel(), TextWatcher {

    override fun afterTextChanged(s: Editable?) {
//        fragment.alertDialog?.getButton(BUTTON_POSITIVE)?.isEnabled =!TextUtils.isEmpty(s?.trim())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    fun onClickPlay(view: View) {
        fragment.showCreateSessionDialog()
//        Toast.makeText(view.context, "Test", Toast.LENGTH_SHORT).show()
//        view.context.startActivity(Intent(view.context, SessionActivity::class.java))
//        fragment.alertDialog?.show()
    }

}