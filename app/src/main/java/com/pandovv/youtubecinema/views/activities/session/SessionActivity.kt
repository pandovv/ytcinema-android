package com.pandovv.youtubecinema.views.activities.session

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.application.App
import com.pandovv.youtubecinema.databinding.ActivitySessionBinding
import com.pandovv.youtubecinema.network.items.Session
import com.pandovv.youtubecinema.network.service.YouTubeAPI.Companion.API_KEY
import com.pandovv.youtubecinema.views.activities.base.BaseActivity
import com.pandovv.youtubecinema.views.listeners.YouTubeActionListener

class SessionActivity : BaseActivity(), SessionActivityImpl {

    companion object {
        const val INTENT_SESSION_DATA = "INTENT_SESSION_DATA"
    }

    private val session: Session?
        get() = intent?.getSerializableExtra(INTENT_SESSION_DATA) as Session?

    override val viewModel = SessionViewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivitySessionBinding>(this, R.layout.activity_session)
        binding.viewModel = viewModel
        binding.session = session
        initYouTubePlayer()
    }

    private fun initYouTubePlayer() {
        val session = session ?: return
        val youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance()
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_youtube_player, youTubePlayerFragment)
                .commit()
        val youTubeActionListener = YouTubeActionListener(session)
        youTubePlayerFragment.initialize(API_KEY, youTubeActionListener)
        val reference = App.getInstance().sessionDatabase
        reference.child(session.id).addValueEventListener(youTubeActionListener)
    }

}
