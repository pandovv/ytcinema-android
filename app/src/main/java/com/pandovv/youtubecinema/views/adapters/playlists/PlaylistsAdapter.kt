package com.pandovv.youtubecinema.views.adapters.playlists

import android.view.View
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.views.base.adapters.BaseListAdapter

class PlaylistsAdapter : BaseListAdapter() {

    override val cardLayout = R.layout.card_playlist

    override fun getHolder(view: View) = PlaylistHolder(view)

}