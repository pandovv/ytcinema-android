package com.pandovv.youtubecinema.views.adapters.videos

import android.view.View
import com.pandovv.youtubecinema.databinding.CardVideoBinding
import com.pandovv.youtubecinema.network.items.Item
import com.pandovv.youtubecinema.network.items.Video
import com.pandovv.youtubecinema.views.adapters.base.BaseHolder
import com.pandovv.youtubecinema.views.fragments.base.interfaces.BaseFragmentListImpl

class VideoHolder(view: View, fragment: BaseFragmentListImpl) : BaseHolder(view) {

    private val viewModel = VideoHolderViewModel(fragment)

    override val binding: CardVideoBinding = CardVideoBinding.bind(view)

    override fun onBind(item: Item) {
        binding.viewModel = viewModel
        binding.item = item as Video
    }

}