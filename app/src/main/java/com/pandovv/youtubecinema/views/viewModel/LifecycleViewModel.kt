package com.pandovv.youtubecinema.views.viewModel

abstract class LifecycleViewModel : LifecycleViewModelImpl {

    override fun onCreate() {}

    override fun onDestroy() {}

    override fun onStart() {}

    override fun onStop() {}

    override fun onResume() {}

    override fun onPause() {}

}