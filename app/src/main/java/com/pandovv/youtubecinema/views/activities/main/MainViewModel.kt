package com.pandovv.youtubecinema.views.activities.main

import android.support.design.widget.BottomNavigationView
import com.ncapdevi.fragnav.FragNavController
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.views.viewModel.LifecycleViewModel

class MainViewModel(private val activity: MainActivityImpl) : LifecycleViewModel() {

    val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> activity.switchTab(FragNavController.TAB1)
            R.id.navigation_sessions -> activity.switchTab(FragNavController.TAB2)
            R.id.navigation_library -> activity.switchTab(FragNavController.TAB3)
        }
        true
    }

}