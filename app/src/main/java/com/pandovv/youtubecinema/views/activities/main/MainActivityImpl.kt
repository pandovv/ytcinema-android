package com.pandovv.youtubecinema.views.activities.main

import com.ncapdevi.fragnav.FragNavController

interface MainActivityImpl {

    fun switchTab(tab: Int): Unit?

    var fragNavController: FragNavController?

}