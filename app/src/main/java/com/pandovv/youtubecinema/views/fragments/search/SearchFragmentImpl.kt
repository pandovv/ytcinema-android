package com.pandovv.youtubecinema.views.fragments.search

import android.support.v7.widget.SearchView
import com.pandovv.youtubecinema.views.fragments.base.interfaces.BaseFragmentListImpl

interface SearchFragmentImpl : BaseFragmentListImpl {

    val searchView: SearchView?

    fun popFragment(): Boolean?

}