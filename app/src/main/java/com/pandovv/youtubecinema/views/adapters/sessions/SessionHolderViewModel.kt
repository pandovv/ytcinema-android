package com.pandovv.youtubecinema.views.adapters.sessions

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.pandovv.youtubecinema.network.items.Session
import com.pandovv.youtubecinema.views.activities.session.SessionActivity

class SessionHolderViewModel {

    fun onItemClick(view: View, session: Session) {
        startSessionActivity(view.context, session)
    }

    private fun startSessionActivity(context: Context?, session: Session?) {
        val intent = Intent(context, SessionActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable(SessionActivity.INTENT_SESSION_DATA, session)
        intent.putExtras(bundle)
        context?.startActivity(intent)
    }

}