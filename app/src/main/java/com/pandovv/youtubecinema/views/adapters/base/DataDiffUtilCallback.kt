package com.pandovv.youtubecinema.views.adapters.base

import android.support.v7.util.DiffUtil
import com.pandovv.youtubecinema.network.items.Item

class DataDiffUtilCallback(private val listOld: List<Item>, private val listNew: List<Item>)
    : DiffUtil.Callback() {

    override fun getOldListSize() = listOld.size

    override fun getNewListSize() = listNew.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            listOld[oldItemPosition].id == listNew[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            listOld[oldItemPosition] == listNew[newItemPosition]

}