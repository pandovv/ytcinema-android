package com.pandovv.youtubecinema.views.fragments.base.interfaces

interface FragmentViewModelScrollListener {

    fun onScrollStopBottom()

}