package com.pandovv.youtubecinema.views.fragments.home

import com.pandovv.youtubecinema.network.models.HomeRepository
import com.pandovv.youtubecinema.views.adapters.videos.VideosAdapter
import com.pandovv.youtubecinema.views.fragments.base.BaseFragmentListViewModel
import com.pandovv.youtubecinema.views.listeners.RecyclerViewScrollListener

class HomeViewModel(fragment: HomeFragmentImpl) : BaseFragmentListViewModel() {

    companion object {
        private const val QUERY = ""
    }

    private val repository = HomeRepository(this)

    override val recyclerViewAdapter = VideosAdapter(fragment)

    override val recyclerViewScrollListener = RecyclerViewScrollListener(this)

    override fun onScrollStopBottom() = repository.search(QUERY, false)

    override fun onRefreshBegin() {
        super.onRefreshBegin()
        repository.search(QUERY, true)
    }

    override fun onCreate() {
        repository.search(QUERY, true)
    }

}