package com.pandovv.youtubecinema.views.fragments.base

abstract class BaseFragmentList : BaseFragment() {

    abstract override val viewModel: BaseFragmentListViewModel

}