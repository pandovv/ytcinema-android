package com.pandovv.youtubecinema.views.listeners

import android.support.v7.widget.RecyclerView
import com.pandovv.youtubecinema.views.fragments.base.interfaces.FragmentViewModelScrollListener

class RecyclerViewScrollListener(private val viewModel: FragmentViewModelScrollListener)
    : RecyclerView.OnScrollListener() {

    companion object {
        private const val ON_SCROLL_BOTTOM_ID = 1
    }

    var isScrollEnabled = true

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (!recyclerView.canScrollVertically(ON_SCROLL_BOTTOM_ID) && isScrollEnabled) {
            isScrollEnabled = false
            viewModel.onScrollStopBottom()
        }
    }



}