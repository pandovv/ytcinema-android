package com.pandovv.youtubecinema.views.listeners

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.GenericTypeIndicator
import com.google.firebase.database.ValueEventListener
import com.pandovv.youtubecinema.network.items.Session
import com.pandovv.youtubecinema.network.responses.SessionsResponse
import com.pandovv.youtubecinema.views.fragments.sessions.SessionsViewModelImpl

class SessionsValueEventListener(private val viewModel: SessionsViewModelImpl) : ValueEventListener {

    override fun onCancelled(error: DatabaseError) {
    }

    override fun onDataChange(dataSnapshot: DataSnapshot) {
//        val t = object : GenericTypeIndicator<HashMap<String, Session>>() {}
        val sessions = dataSnapshot.getValue(SessionsResponse())
        Log.e("sessions", sessions.toString())
        sessions ?: return
        val test = ArrayList<Session>()
        sessions.forEach {
            Log.e("session", it.value.name)
            test.add(it.value)
        }
        viewModel.addItems(test, true)
    }
}