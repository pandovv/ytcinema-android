package com.pandovv.youtubecinema.views.adapters.videos

import android.view.View
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.views.base.adapters.BaseListAdapter
import com.pandovv.youtubecinema.views.fragments.base.interfaces.BaseFragmentListImpl

class VideosAdapter(private val fragment: BaseFragmentListImpl) : BaseListAdapter() {

    override val cardLayout = R.layout.card_video

    override fun getHolder(view: View) = VideoHolder(view, fragment)

}