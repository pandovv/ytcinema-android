package com.pandovv.youtubecinema.views.fragments.preview

import com.pandovv.youtubecinema.views.fragments.base.interfaces.BaseFragmentImpl

interface PreviewFragmentImpl : BaseFragmentImpl {

    fun showCreateSessionDialog()

}