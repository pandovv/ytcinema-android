package com.pandovv.youtubecinema.views.fragments.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.databinding.FragmentHomeBinding
import com.pandovv.youtubecinema.views.fragments.base.BaseFragmentList

class HomeFragment : BaseFragmentList(), HomeFragmentImpl {

    companion object {

        fun newInstance() = HomeFragment()

    }

    override val viewModel = HomeViewModel(this)

    override val optionsMenu = R.menu.menu_main

    override fun getBindingView(inflater: LayoutInflater, container: ViewGroup?): View? {
        val binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

}
