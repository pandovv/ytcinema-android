package com.pandovv.youtubecinema.views.fragments.preview.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.application.App
import com.pandovv.youtubecinema.databinding.FragmentPreviewDialogBinding
import com.pandovv.youtubecinema.network.items.Session
import com.pandovv.youtubecinema.network.items.Video
import com.pandovv.youtubecinema.views.activities.session.SessionActivity
import com.pandovv.youtubecinema.views.activities.session.SessionActivity.Companion.INTENT_SESSION_DATA
import com.pandovv.youtubecinema.views.fragments.preview.PreviewFragment.Companion.BUNDLE_VIDEO_ITEM
import com.pandovv.youtubecinema.views.listeners.YouTubeActionListener.Status.STOP

class PreviewDialogFragment : DialogFragment(), CreateSessionImpl {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentPreviewDialogBinding.inflate(inflater, container, false)
        binding.viewModel = PreviewDialogViewModel(this)
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context!!)
        val view = activity?.layoutInflater?.inflate(R.layout.fragment_preview_dialog, null)
        builder.setView(view)
                .setPositiveButton(R.string.text_create) { dialog, _ -> onClickPositive(view, dialog) }
                .setNegativeButton(R.string.text_cancel) { dialog, _ -> dialog.cancel() }
        return builder.create()
    }

    private fun onClickPositive(view: View?, dialog: DialogInterface) {
        val name = view?.findViewById<EditText>(R.id.edit_session_name)?.text?.toString() ?: return dialog.cancel()
        val password = view.findViewById<EditText>(R.id.edit_session_pass)?.text?.toString() ?: return dialog.cancel()
        val video = arguments?.getSerializable(BUNDLE_VIDEO_ITEM) as Video? ?: return dialog.cancel()
        if (name.isNotEmpty()) {
            val session = createSession(name, password, video.title, video.id, video.snippetUrl)
            startSessionActivity(view.context, session)
        } else dialog.cancel()
    }

    private fun createSession(name: String, password: String, videoTitle: String, videoId: String, snippetUrl: String): Session? {
        val reference = App.getInstance().sessionDatabase
        val sessionId = reference.push().key
        if (sessionId != null) {
            val session = Session(sessionId, name, password, videoTitle, videoId, snippetUrl, STOP, 0)
            reference.child(sessionId).setValue(session)
            return session
        }
        return null
    }

    private fun startSessionActivity(context: Context?, session: Session?) {
        val intent = Intent(context, SessionActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable(INTENT_SESSION_DATA, session)
        intent.putExtras(bundle)
        context?.startActivity(intent)
    }

}