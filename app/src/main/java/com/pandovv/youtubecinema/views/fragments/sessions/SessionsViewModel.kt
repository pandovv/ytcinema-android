package com.pandovv.youtubecinema.views.fragments.sessions

import com.pandovv.youtubecinema.application.App
import com.pandovv.youtubecinema.views.adapters.sessions.SessionsAdapter
import com.pandovv.youtubecinema.views.fragments.base.BaseFragmentListViewModel
import com.pandovv.youtubecinema.views.listeners.RecyclerViewScrollListener
import com.pandovv.youtubecinema.views.listeners.SessionsValueEventListener

class SessionsViewModel(private val fragment: SessionsFragmentImpl) : BaseFragmentListViewModel(),
        SessionsViewModelImpl {

    override val recyclerViewAdapter = SessionsAdapter()
    override val recyclerViewScrollListener: RecyclerViewScrollListener? = null

    private val reference = App.getInstance().sessionDatabase
    private val valueEventListener = SessionsValueEventListener(this)

    override fun onRefreshBegin() {
        super.onRefreshBegin()
        super.onRefreshComplete()
    }

    override fun onResume() {
        super.onResume()
        reference.addValueEventListener(valueEventListener)
    }

    override fun onPause() {
        super.onPause()
        reference.removeEventListener(valueEventListener)
    }

}