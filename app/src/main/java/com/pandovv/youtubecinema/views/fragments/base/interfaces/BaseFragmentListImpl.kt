package com.pandovv.youtubecinema.views.fragments.base.interfaces

import com.pandovv.youtubecinema.views.activities.main.FragmentActivityImpl

interface BaseFragmentListImpl : BaseFragmentImpl {

    val activity: FragmentActivityImpl

}