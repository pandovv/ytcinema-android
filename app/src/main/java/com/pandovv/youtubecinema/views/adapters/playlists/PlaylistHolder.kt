package com.pandovv.youtubecinema.views.adapters.playlists

import android.view.View
import com.pandovv.youtubecinema.databinding.CardPlaylistBinding
import com.pandovv.youtubecinema.network.items.Item
import com.pandovv.youtubecinema.network.items.Playlist
import com.pandovv.youtubecinema.views.adapters.base.BaseHolder

class PlaylistHolder(view: View) : BaseHolder(view) {

    private val viewModel = PlaylistHolderViewModel()

    override val binding: CardPlaylistBinding = CardPlaylistBinding.bind(view)

    override fun onBind(item: Item) {
        binding.viewModel = viewModel
        binding.item = item as Playlist
    }

}