package com.pandovv.youtubecinema.views.fragments.library

import com.pandovv.youtubecinema.views.adapters.playlists.PlaylistsAdapter
import com.pandovv.youtubecinema.views.fragments.base.BaseFragmentListViewModel
import com.pandovv.youtubecinema.views.listeners.RecyclerViewScrollListener

class LibraryViewModel(private val fragment: LibraryFragmentImpl) : BaseFragmentListViewModel() {

    override val recyclerViewAdapter = PlaylistsAdapter()

    override val recyclerViewScrollListener: RecyclerViewScrollListener? = null

}