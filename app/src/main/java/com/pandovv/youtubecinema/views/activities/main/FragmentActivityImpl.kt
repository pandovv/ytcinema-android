package com.pandovv.youtubecinema.views.activities.main

import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.widget.Toolbar

interface FragmentActivityImpl {

    fun setSupportActionBar(toolbar: Toolbar?)

    fun getSupportActionBar(): ActionBar?

//    fun getWindow(): Window?

    fun showSearchFragment(bundle: Bundle?): Unit?

    fun showPreviewFragment(bundle: Bundle?): Unit?

    fun popFragment(): Boolean?

}