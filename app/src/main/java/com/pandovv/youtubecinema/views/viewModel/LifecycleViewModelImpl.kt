package com.pandovv.youtubecinema.views.viewModel

interface LifecycleViewModelImpl {

    fun onCreate()

    fun onDestroy()

    fun onStart()

    fun onStop()

    fun onResume()

    fun onPause()

}