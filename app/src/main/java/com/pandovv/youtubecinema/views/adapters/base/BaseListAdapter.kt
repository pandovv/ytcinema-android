package com.pandovv.youtubecinema.views.base.adapters

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pandovv.youtubecinema.network.items.Item
import com.pandovv.youtubecinema.views.adapters.base.BaseHolder
import com.pandovv.youtubecinema.views.adapters.base.DataDiffUtilCallback

abstract class BaseListAdapter : RecyclerView.Adapter<BaseHolder>() {

    private var items = ArrayList<Item>()

    protected abstract val cardLayout: Int

    protected abstract fun getHolder(view: View): BaseHolder

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: BaseHolder, position: Int) = holder.bind(items, position)

    override fun onCreateViewHolder(parent: ViewGroup, position: Int) =
            getHolder(LayoutInflater.from(parent.context)
                    .inflate(cardLayout, parent, false))

    fun addItems(itemsNew: List<Item>, clear: Boolean) {
        clearItems(clear)
        val itemsAll = ArrayList(items + itemsNew)
        val diffResult = DiffUtil.calculateDiff(DataDiffUtilCallback(items, itemsAll))
        items = itemsAll
        diffResult.dispatchUpdatesTo(this)
    }

    private fun clearItems(clear: Boolean) {
        if (clear) {
            items.clear()
            notifyDataSetChanged()
        }
    }

}