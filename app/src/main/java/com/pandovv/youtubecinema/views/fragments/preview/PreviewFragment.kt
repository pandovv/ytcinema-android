package com.pandovv.youtubecinema.views.fragments.preview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.databinding.FragmentPreviewBinding
import com.pandovv.youtubecinema.network.items.Video
import com.pandovv.youtubecinema.views.fragments.base.BaseFragment
import com.pandovv.youtubecinema.views.fragments.preview.dialog.PreviewDialogFragment

class PreviewFragment : BaseFragment(), PreviewFragmentImpl {

    companion object {

        const val BUNDLE_VIDEO_ITEM = "BUNDLE_VIDEO_ITEM"

        fun newInstance() = PreviewFragment()

    }

    override val optionsMenu = R.menu.menu_preview

    override val viewModel = PreviewViewModel(this)

    private val video: Video?
        get() = arguments?.getSerializable(BUNDLE_VIDEO_ITEM) as Video?

    override fun getBindingView(inflater: LayoutInflater, container: ViewGroup?): View? {
        val binding = FragmentPreviewBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.item = video
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.getSupportActionBar()?.title = video?.title
        activity.getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
    }

    override fun showCreateSessionDialog() {
        val dialog = PreviewDialogFragment()
        val bundle = Bundle()
        bundle.putSerializable(BUNDLE_VIDEO_ITEM, video)
        dialog.arguments = bundle
        dialog.show(fragmentManager, null)
    }

}