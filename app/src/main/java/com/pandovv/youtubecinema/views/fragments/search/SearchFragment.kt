package com.pandovv.youtubecinema.views.fragments.search

import android.support.v7.widget.SearchView
import android.view.*
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.databinding.FragmentSearchBinding
import com.pandovv.youtubecinema.views.fragments.base.BaseFragmentList

class SearchFragment : BaseFragmentList(), SearchFragmentImpl {

    companion object {

        fun newInstance() = SearchFragment()

    }

    override val viewModel = SearchViewModel(this)

    override val optionsMenu = R.menu.menu_search

    override var searchView: SearchView? = null

    override fun getBindingView(inflater: LayoutInflater, container: ViewGroup?): View? {
        val binding = FragmentSearchBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        val searchItem = menu?.findItem(R.id.action_search)
        searchItem?.expandActionView()
        searchItem?.setOnActionExpandListener(viewModel)
        searchView = searchItem?.actionView as SearchView?
        searchView?.setOnQueryTextListener(viewModel)
    }

    override fun popFragment() = activity.popFragment()

}