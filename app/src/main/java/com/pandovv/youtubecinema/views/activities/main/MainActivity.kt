package com.pandovv.youtubecinema.views.activities.main

import android.content.Intent
import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.ImageView
import com.ncapdevi.fragnav.FragNavController
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.views.activities.base.BaseActivity
import com.pandovv.youtubecinema.databinding.ActivityMainBinding
import com.pandovv.youtubecinema.network.items.Session
import com.pandovv.youtubecinema.views.activities.session.SessionActivity
import com.pandovv.youtubecinema.views.fragments.home.HomeFragment
import com.pandovv.youtubecinema.views.fragments.library.LibraryFragment
import com.pandovv.youtubecinema.views.fragments.preview.PreviewFragment
import com.pandovv.youtubecinema.views.fragments.search.SearchFragment
import com.pandovv.youtubecinema.views.fragments.sessions.SessionsFragment
import com.squareup.picasso.Picasso

class MainActivity : BaseActivity(), FragmentActivityImpl, MainActivityImpl {

    override val viewModel = MainViewModel(this)

    override var fragNavController: FragNavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.viewModel = viewModel
        initFragmentsNav(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        if (outState != null) {
            fragNavController?.onSaveInstanceState(outState)
        }
    }

    override fun onBackPressed() {
        if (fragNavController?.isRootFragment == true) {
            super.onBackPressed()
        } else {
            fragNavController?.popFragment()
        }
    }

    override fun showSearchFragment(bundle: Bundle?) {
        val fragment = SearchFragment.newInstance()
        fragment.arguments = bundle
        fragNavController?.pushFragment(fragment)
    }

    override fun showPreviewFragment(bundle: Bundle?) {
        val fragment = PreviewFragment.newInstance()
        fragment.arguments = bundle
        fragNavController?.pushFragment(fragment)
    }

    override fun popFragment() = fragNavController?.popFragment()

    private fun initFragmentsNav(savedInstanceState: Bundle?) {
        val builder = FragNavController.newBuilder(savedInstanceState, supportFragmentManager, R.id.fragment_container)
        val fragments = ArrayList<Fragment>()
        fragments.add(HomeFragment.newInstance())
        fragments.add(SessionsFragment.newInstance())
        fragments.add(LibraryFragment.newInstance())
        builder.rootFragments(fragments)
        fragNavController = builder.build()
    }

    override fun switchTab(tab: Int) {
        fragNavController?.switchTab(tab)
    }

    companion object {

        @BindingAdapter("imageUrl")
        @JvmStatic
        fun loadImage(view: ImageView, url: String) {
            Picasso.get().load(url).into(view)
        }

    }

}
