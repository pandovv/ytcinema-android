package com.pandovv.youtubecinema.views.adapters.base

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View
import com.pandovv.youtubecinema.network.items.Item

abstract class BaseHolder(view: View) : RecyclerView.ViewHolder(view) {

    protected abstract val binding: ViewDataBinding

    abstract fun onBind(item: Item)

    fun bind(items: List<Item>, position: Int) {
        onBind(items[position])
        binding.executePendingBindings()
    }

}