package com.pandovv.youtubecinema.views.fragments.base

import android.databinding.ObservableField
import com.pandovv.youtubecinema.network.items.Item
import com.pandovv.youtubecinema.views.viewModel.LifecycleViewModel
import com.pandovv.youtubecinema.views.base.adapters.BaseListAdapter
import com.pandovv.youtubecinema.views.fragments.base.interfaces.BaseFragmentListViewModelImpl
import com.pandovv.youtubecinema.views.fragments.base.interfaces.FragmentViewModelRepository
import com.pandovv.youtubecinema.views.fragments.base.interfaces.FragmentViewModelScrollListener
import com.pandovv.youtubecinema.views.listeners.RecyclerViewScrollListener

abstract class BaseFragmentListViewModel : LifecycleViewModel(),
        FragmentViewModelScrollListener, FragmentViewModelRepository, BaseFragmentListViewModelImpl {

    abstract val recyclerViewAdapter: BaseListAdapter

    abstract val recyclerViewScrollListener: RecyclerViewScrollListener?

    val isRefreshing = ObservableField<Boolean>(false)

    override fun onRefreshBegin() {
        isRefreshing.set(true)
        recyclerViewScrollListener?.isScrollEnabled = false
    }

    override fun onRefreshComplete() {
        isRefreshing.set(false)
        recyclerViewScrollListener?.isScrollEnabled = true
    }

    override fun onRefreshError() {

    }

    override fun onScrollStopBottom() {

    }

    override fun addItems(items: List<Item>, clear: Boolean)
            = recyclerViewAdapter.addItems(items, clear)

}