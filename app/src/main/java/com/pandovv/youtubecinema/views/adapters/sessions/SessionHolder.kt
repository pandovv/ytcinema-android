package com.pandovv.youtubecinema.views.adapters.sessions

import android.view.View
import com.pandovv.youtubecinema.databinding.CardSessionBinding
import com.pandovv.youtubecinema.network.items.Item
import com.pandovv.youtubecinema.network.items.Session
import com.pandovv.youtubecinema.views.adapters.base.BaseHolder

class SessionHolder(view: View) : BaseHolder(view) {

    private val viewModel = SessionHolderViewModel()

    override val binding: CardSessionBinding = CardSessionBinding.bind(view)

    override fun onBind(item: Item) {
        binding.viewModel = viewModel
        binding.item = item as Session
    }

}