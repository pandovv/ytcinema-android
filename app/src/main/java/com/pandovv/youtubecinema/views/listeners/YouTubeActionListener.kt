package com.pandovv.youtubecinema.views.listeners

import android.util.Log
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.pandovv.youtubecinema.application.App
import com.pandovv.youtubecinema.network.items.Session

class YouTubeActionListener(private val session: Session) :
        YouTubePlayer.OnInitializedListener, YouTubePlayer.PlaybackEventListener,
        YouTubePlayer.PlayerStateChangeListener, ValueEventListener {

    companion object Status {
        private const val PLAYER_STATUS = "playerStatus"
        private const val PLAYER_TIMELINE = "playerTimeline"
        const val STOP = "stop"
        const val PLAY = "play"
        const val PAUSE = "pause"
    }

    private var youTubePlayer: YouTubePlayer? = null

    override fun onInitializationSuccess(provider: YouTubePlayer.Provider?, player: YouTubePlayer?, isRestored: Boolean) {
        Log.e("YouTubeActionListener", "onInitializationSuccess (${session.playerStatus})")
        youTubePlayer = player ?: return
        player.setPlaybackEventListener(this)
        player.setPlayerStateChangeListener(this)
        player.cueVideo(session.videoId)
        setClientSession(session.playerStatus, session.playerTimeline)
    }

    override fun onInitializationFailure(provider: YouTubePlayer.Provider?, result: YouTubeInitializationResult?) {
        Log.e("YouTubeActionListener", "onInitializationFailure (${session.playerStatus})")
    }

    override fun onSeekTo(seek: Int) {
        Log.e("YouTubeActionListener", "onSeekTo (${session.playerStatus})")
        setServerSession(session.id, session.playerStatus, seek)
    }

    override fun onBuffering(isBuffering: Boolean) {
        Log.e("YouTubeActionListener", "onBuffering (${session.playerStatus})")
    }

    override fun onPlaying() {
        Log.e("YouTubeActionListener", "onPlaying (${session.playerStatus})")
        val youTubePlayer = youTubePlayer ?: return
        setServerSession(session.id, PLAY, youTubePlayer.currentTimeMillis)
    }

    override fun onStopped() {
        Log.e("YouTubeActionListener", "onStopped (${session.playerStatus})")
        val youTubePlayer = youTubePlayer ?: return
        setServerSession(session.id, STOP, youTubePlayer.currentTimeMillis)
    }

    override fun onPaused() {
        Log.e("YouTubeActionListener", "onPaused (${session.playerStatus})")
        val youTubePlayer = youTubePlayer ?: return
        setServerSession(session.id, PAUSE, youTubePlayer.currentTimeMillis)
    }

    override fun onAdStarted() {
        Log.e("YouTubeActionListener", "onAdStarted (${session.playerStatus})")
//        activity.viewModel.setServerSession(session.id, "pause", youTubePlayer.currentTimeMillis)//
    }

    override fun onLoading() {
        Log.e("YouTubeActionListener", "onLoading (${session.playerStatus})")
//        activity.viewModel.setServerSession(session.id, "pause", youTubePlayer.currentTimeMillis)//
    }

    override fun onVideoStarted() {
        Log.e("YouTubeActionListener", "onVideoStarted (${session.playerStatus})")
    }

    override fun onLoaded(videoId: String?) {
        Log.e("YouTubeActionListener", "onLoaded (${session.playerStatus})")
//        activity.viewModel.setServerSession(session.id, "play", youTubePlayer.currentTimeMillis)//
    }

    override fun onVideoEnded() {
        Log.e("YouTubeActionListener", "onVideoEnded (${session.playerStatus})")
//        activity.viewModel.setServerSession(session.id, "stop", youTubePlayer.currentTimeMillis)//
    }

    override fun onError(reason: YouTubePlayer.ErrorReason?) {
        Log.e("YouTubeActionListener", "onError (${session.playerStatus})\n$reason")
    }

    override fun onCancelled(error: DatabaseError) {
        Log.e("YouTubeActionListener", "onCancelled (${session.playerStatus})", error.toException())
    }

    override fun onDataChange(dataSnapshot: DataSnapshot) {
        Log.e("YouTubeActionListener", "onDataChange (${session.playerStatus})")
        val session = dataSnapshot.getValue(Session::class.java) ?: return
        setClientSession(session.playerStatus, session.playerTimeline)
    }

    private fun setClientSession(status: String, timeline: Int) {
        val youTubePlayer = youTubePlayer ?: return
        if (status == PLAY && !youTubePlayer.isPlaying) {
            session.playerStatus = status
            session.playerTimeline = timeline
            youTubePlayer.play()
            youTubePlayer.seekToMillis(timeline)
        }
        else if (status == PAUSE && youTubePlayer.isPlaying) {
            session.playerStatus = status
            session.playerTimeline = timeline
            youTubePlayer.pause()
            youTubePlayer.seekToMillis(timeline)
        }
    }

    private fun setServerSession(id: String, status: String, timeline: Int) {
        val reference = App.getInstance().sessionDatabase
        val session = reference.child(id)
        session.child(PLAYER_STATUS).setValue(status)
        session.child(PLAYER_TIMELINE).setValue(timeline)
    }

}