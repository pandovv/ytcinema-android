package com.pandovv.youtubecinema.views.adapters.sessions

import android.view.View
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.views.base.adapters.BaseListAdapter

class SessionsAdapter : BaseListAdapter() {

    override val cardLayout = R.layout.card_session

    override fun getHolder(view: View) = SessionHolder(view)

}