package com.pandovv.youtubecinema.views.fragments.search

import android.support.v7.widget.SearchView
import android.view.MenuItem
import com.pandovv.youtubecinema.network.models.SearchRepository
import com.pandovv.youtubecinema.views.adapters.videos.VideosAdapter
import com.pandovv.youtubecinema.views.fragments.base.BaseFragmentListViewModel
import com.pandovv.youtubecinema.views.listeners.RecyclerViewScrollListener

class SearchViewModel(private val fragment: SearchFragmentImpl)
    : BaseFragmentListViewModel(), SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener {

    companion object {
        private var QUERY = ""
    }

    private val repository = SearchRepository(this)

    override val recyclerViewAdapter = VideosAdapter(fragment)

    override val recyclerViewScrollListener: RecyclerViewScrollListener? = null

    override fun onScrollStopBottom() = repository.search(QUERY, false)

    override fun onRefreshBegin() {
        super.onRefreshBegin()
        if (QUERY.isNotEmpty()) {
            repository.search(QUERY, true)
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (!query.isNullOrEmpty()) {
            fragment.searchView?.clearFocus()
            repository.search(query!!, true)
            QUERY = query
            return true
        }
        return false
    }

    override fun onQueryTextChange(query: String?): Boolean {
        return true
    }

    override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
        return true
    }

    override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
        fragment.popFragment()
        return true
    }

}