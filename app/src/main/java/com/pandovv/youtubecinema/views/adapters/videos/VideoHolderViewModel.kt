package com.pandovv.youtubecinema.views.adapters.videos

import android.databinding.ObservableField
import android.os.Bundle
import android.support.v7.widget.PopupMenu
import android.view.View
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.network.items.Video
import com.pandovv.youtubecinema.views.fragments.base.interfaces.BaseFragmentListImpl
import com.pandovv.youtubecinema.views.fragments.preview.PreviewFragment

class VideoHolderViewModel(private val fragment: BaseFragmentListImpl) {

    val isExpanded = ObservableField<Boolean>(false)

    fun onItemClick(view: View, video: Video) {
//        val bundle = Bundle()
//        bundle.putSerializable(PreviewFragment.BUNDLE_VIDEO_ITEM, video)
//        fragment.activity.showPreviewFragment(bundle)
        val test = isExpanded.get() ?: return
        isExpanded.set(!test)
    }

    fun onItemClickOptions(view: View, video: Video) {
        val popup = PopupMenu(view.context, view)
        popup.inflate(R.menu.menu_preview)
        popup.setOnMenuItemClickListener {
            true
        }
        popup.show()
    }

}