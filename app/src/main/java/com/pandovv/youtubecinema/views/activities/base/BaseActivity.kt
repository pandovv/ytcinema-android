package com.pandovv.youtubecinema.views.activities.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.pandovv.youtubecinema.views.viewModel.LifecycleViewModel

abstract class BaseActivity : AppCompatActivity() {

    protected abstract val viewModel: LifecycleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onCreate()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDestroy()
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }

    override fun onStop() {
        super.onStop()
        viewModel.onStop()
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
    }

}