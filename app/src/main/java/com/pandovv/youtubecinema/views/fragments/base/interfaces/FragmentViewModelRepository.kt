package com.pandovv.youtubecinema.views.fragments.base.interfaces

import com.pandovv.youtubecinema.network.items.Item

interface FragmentViewModelRepository {

    fun onRefreshError()

    fun onRefreshComplete()

    fun addItems(items: List<Item>, clear: Boolean = false)

}