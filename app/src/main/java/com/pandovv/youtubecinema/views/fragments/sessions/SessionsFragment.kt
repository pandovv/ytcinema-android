package com.pandovv.youtubecinema.views.fragments.sessions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.databinding.FragmentSessionsBinding
import com.pandovv.youtubecinema.views.fragments.base.BaseFragmentList

class SessionsFragment : BaseFragmentList(), SessionsFragmentImpl {

    companion object {

        fun newInstance() = SessionsFragment()

    }

    override val viewModel = SessionsViewModel(this)

    override val optionsMenu = R.menu.menu_main

    override fun getBindingView(inflater: LayoutInflater, container: ViewGroup?): View? {
        val binding = FragmentSessionsBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

}