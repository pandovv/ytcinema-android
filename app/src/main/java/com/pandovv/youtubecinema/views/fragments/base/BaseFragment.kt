package com.pandovv.youtubecinema.views.fragments.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.views.viewModel.LifecycleViewModel
import com.pandovv.youtubecinema.views.activities.main.FragmentActivityImpl
import com.pandovv.youtubecinema.views.activities.main.MainActivity
import com.pandovv.youtubecinema.views.fragments.base.interfaces.BaseFragmentImpl
import kotlinx.android.synthetic.main.toolbar.view.*

abstract class BaseFragment : Fragment(), BaseFragmentImpl {

    val activity: FragmentActivityImpl
        get() = getActivity() as MainActivity

    abstract fun getBindingView(inflater: LayoutInflater, container: ViewGroup?): View?

    protected abstract val optionsMenu: Int

    protected abstract val viewModel: LifecycleViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = getBindingView(inflater, container)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity.setSupportActionBar(view.toolbar)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(optionsMenu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> activity.popFragment()
            R.id.action_search -> activity.showSearchFragment(null)
            R.id.action_speak -> { }
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onCreate()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDestroy()
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }

    override fun onStop() {
        super.onStop()
        viewModel.onStop()
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
    }

}