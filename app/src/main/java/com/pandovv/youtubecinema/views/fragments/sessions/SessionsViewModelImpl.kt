package com.pandovv.youtubecinema.views.fragments.sessions

import com.pandovv.youtubecinema.network.items.Item

interface SessionsViewModelImpl {

    fun addItems(items: List<Item>, clear: Boolean = false)

}