package com.pandovv.youtubecinema.views.fragments.library

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pandovv.youtubecinema.R
import com.pandovv.youtubecinema.databinding.FragmentLibraryBinding
import com.pandovv.youtubecinema.views.fragments.base.BaseFragmentList

class LibraryFragment : BaseFragmentList(), LibraryFragmentImpl {

    companion object {

        fun newInstance() = LibraryFragment()

    }

    override val viewModel = LibraryViewModel(this)

    override val optionsMenu = R.menu.menu_main

    override fun getBindingView(inflater: LayoutInflater, container: ViewGroup?): View? {
        val binding = FragmentLibraryBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

}
